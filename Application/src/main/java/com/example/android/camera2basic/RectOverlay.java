package com.example.android.camera2basic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class RectOverlay extends View {
    private Paint paint = new Paint();

    public RectOverlay(Context context) {
        super(context);
    }

    public RectOverlay(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RectOverlay(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /*private Rect rect;*/
    private float LEFT = 1.0f / 6;
    private float TOP =  1.0f / 4;
    private float RIGHT = 5.0f / 6;
    private float BOTTOM = 3.0f / 4;

    @Override
    protected void onDraw(Canvas canvas) { // Override the onDraw() Method
        super.onDraw(canvas);

        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        paint.setStrokeWidth(10);

        //center
        int x = canvas.getWidth();
        int y = canvas.getHeight();

        //draw guide box
        canvas.drawRect((int) (x * LEFT), (int) (y * TOP), (int) (x * RIGHT), (int) (y * BOTTOM), paint);
    }

/*    public Rect getRect() {
        return new Rect((int)(2000 * LEFT - 1000),(int) (2000 * TOP - 1000), (int)(2000 * RIGHT - 1000), (int)(2000 * BOTTOM - 1000));
    }*/

}
